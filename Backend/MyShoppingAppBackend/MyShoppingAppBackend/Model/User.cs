﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyShoppingAppBackend.Model
{
    public partial class User
    {
        public string Uid { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public double? Credit { get; set; }
        public string Token { get; set; }
    }
}
