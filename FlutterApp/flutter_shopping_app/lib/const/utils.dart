import 'package:flutter/material.dart';

void showShackBarWithViewBag(BuildContext context, String s) {
  Scaffold.of(context).showSnackBar(SnackBar(content: Text('$s'),
    action: SnackBarAction(label: 'View Bag', onPressed: () => Navigator.of(context).pushNamed('/CartDetail'),),));
}

void showOnlyShackBar(BuildContext context, String s) {
  Scaffold.of(context).showSnackBar(SnackBar(content: Text('$s'),
  ));
}