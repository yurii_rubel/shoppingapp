import 'dart:io';
import 'dart:math';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_shopping_app/firebase/notification_handler.dart';

class FirebaseNotifications{
  FirebaseMessaging _messaging;
  BuildContext myContext;

  static Future fcmBackgroundMessageHandler(Map<String,dynamic> message) async{
    dynamic data = message['data'];
    showNotification(data['title'],data['body']);
    return Future<void>.value();
  }

  void setupFirebase(BuildContext context)
  {
    myContext = context;
    _messaging = FirebaseMessaging();
    NotificationHandler.initNotification(context);
    firebaseCloudMessagingListener(context);
  }

void firebaseCloudMessagingListener(BuildContext context) {
  // Request permission
  _messaging.requestNotificationPermissions(
    const  IosNotificationSettings(sound: true, badge: true, alert: true)
  );
  _messaging.onIosSettingsRegistered.listen((event) => print('Registered $event'));

  //Get token
  _messaging.getToken().then((token) => print('Token $token'));

  Future.delayed(Duration(seconds: 1),(){
    _messaging.configure(onBackgroundMessage: Platform.isIOS ? null: fcmBackgroundMessageHandler,
    onMessage: (Map<String,dynamic> message) async{
      if(Platform.isAndroid)
      showNotification(message['data']['title'], message['data']['body']);
      else if(Platform.isIOS)
        showNotification(message['notification']['title'], message['notification']['body']);
    },
    onResume: (Map<String,dynamic> message) async{
      if(Platform.isIOS)
        showDialog(
            context: myContext,
            builder: (context) => CupertinoAlertDialog(
              title: Text(message['title']),
              content: Text( message['body']),
              actions: [
                CupertinoDialogAction(child: Text('OK'), isDefaultAction: true,
                  onPressed: (){
                    Navigator.of(context, rootNavigator: true).pop();
                  },)
              ],
            )
        );
    }, onLaunch: (Map<String,dynamic> message) async{
          if(Platform.isIOS)
            showDialog(
                context: myContext,
                builder: (context) => CupertinoAlertDialog(
                  title: Text(message['title']),
                  content: Text( message['body']),
                  actions: [
                    CupertinoDialogAction(child: Text('OK'), isDefaultAction: true,
                      onPressed: (){
                        Navigator.of(context, rootNavigator: true).pop();
                      },)
                  ],
                )
            );
        });
  });
}

  static void showNotification(title, content) async{
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'com.example.flutter_shopping_app', 'Flutter Shopping App', 'Description',
    autoCancel: false,
        ongoing: true,
    importance: Importance.max,
        priority: Priority.high);
    var iosPlatformSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics, iOS: iosPlatformSpecifics);

    await NotificationHandler.flutterLocalNotificationPlugin.show(
      Random().nextInt(1000),
      title,
      content,
      platformChannelSpecifics,
      payload: 'Payload'
    );
  }
}