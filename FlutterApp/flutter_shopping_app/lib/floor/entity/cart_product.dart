import 'package:floor/floor.dart';

@entity
class Cart{
  @primaryKey
  final int productId;

  final String uid, name, imageUrl, size, code;
  double price;
  int quantity;

  Cart(
      {this.productId,
      this.uid,
      this.name,
      this.imageUrl,
      this.size,
      this.code,
      this.price,
      this.quantity});
}