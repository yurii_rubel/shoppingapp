class UserModel{
  String uid,name,phone,token;
  int credit;

  UserModel(this.uid,this.name,this.phone,this.credit,this.token);

  UserModel.fromJson(Map<String, dynamic> json) {
    uid = json['uid'];
    name = json['name'];
    phone = json['phone'];
    token = json['token'];
    credit = json['credit'];

    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['uid'] = this.uid;
      data['name'] = this.name;
      data['phone'] = this.phone;
      data['token'] = this.token;
      data['credit'] = this.credit;
      return data;
    }
  }
}