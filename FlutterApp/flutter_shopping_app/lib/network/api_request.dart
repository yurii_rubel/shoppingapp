import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_shopping_app/const/api_const.dart';
import 'package:flutter_shopping_app/model/UserModel.dart';
import 'package:flutter_shopping_app/model/category.dart';
import 'package:flutter_shopping_app/model/feature_image.dart';
import 'package:flutter_shopping_app/model/product.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_shopping_app/model/banner.dart';

Product parseProductDetail(String responseBody){
  var l = json.decode(responseBody) as dynamic;
  var product = Product.fromJson(l);
  return product;
}

List<Product> parseProduct(String responseBody){
  var l = json.decode(responseBody) as List<dynamic>;
  var products = l.map((model) => Product.fromJson(model)).toList();
  return products;
}

List<MyCategory> parseCategory(String responseBody){
  var l = json.decode(responseBody) as List<dynamic>;
  var categories = l.map((model) => MyCategory.fromJson(model)).toList();
  return categories;
}

List<MyBanner> parseBanner(String responseBody){
  var l = json.decode(responseBody) as List<dynamic>;
  var banners = l.map((model) => MyBanner.fromJson(model)).toList();
  return banners;
}

List<FeatureImg> parseFeatureImage(String responseBody){
  var l = json.decode(responseBody) as List<dynamic>;
  var featureImages = l.map((model) => FeatureImg.fromJson(model)).toList();
  return featureImages;
}

Future<List<MyBanner>> fetchBanner() async
{
  final response = await http.get('$mainUrl$bannerUrl');
  if(response.statusCode == 200)
    return compute(parseBanner,response.body);
  else if(response.statusCode == 404)
    throw Exception('Not found');
  else
    throw Exception('Cannot get Banner');
}

Future<List<FeatureImg>> fetchFeatureImages() async
{
  final response = await http.get('$mainUrl$featureUrl');
  if (response.statusCode == 200)
    return compute(parseFeatureImage,response.body);
  else if(response.statusCode == 404)
    throw Exception('Not found');
  else
    throw Exception('Cannot get FeatureImg');
}

Future<List<MyCategory>> fetchCategories() async
{
  final response = await http.get('$mainUrl$categoriesUrl');
  if (response.statusCode == 200)
    return compute(parseCategory,response.body);
  else if(response.statusCode == 404)
    throw Exception('Not found');
  else
    throw Exception('Cannot get Categories');
}

Future<List<Product>> fetchProductsBySubCategory(id) async
{
  final response = await http.get('$mainUrl$productUrl/$id');
  if (response.statusCode == 200)
    return compute(parseProduct,response.body);
  else if(response.statusCode == 404)
    throw Exception('Not found');
  else
    throw Exception('Cannot get Product');
}

Future<Product> fetchProductsDetail(id) async
{
  final response = await http.get('$mainUrl$productDetail/$id');
  if (response.statusCode == 200)
    return compute(parseProductDetail,response.body);
  else if(response.statusCode == 404)
    throw Exception('Not found');
  else
    throw Exception('Cannot get Product Detail');
}

Future<String> findUser(id,token) async
{
  final response = await http.get('$mainUrl$userPath/$id',
  headers: {
    'Authorization':'Bearer $token'
  });
  if (response.statusCode == 200)
    return response.body; //Return user information
  else if(response.statusCode == 404)
    return 'User Not found';
  else
    throw Exception('Cannot get User by Id');
}

createUserApi(String key,String uid,String name,String phone,String address) async{
  var body={
    'uid':uid,
    'name':name,
    'phone':phone,
    'address':address
  };
  var res = await http.post('$mainUrl$userPath',headers: {
    "content-type":"application/json",
    "accept":"application/json",
    "Authorization":"Bearer $key"
  },body: json.encode(body));
    if(res.statusCode == 201) return 'Created';
    else if(res.statusCode == 209) return 'Conflict';
    else return res;
}

updateTokenApi(String key,String uid,UserModel userModel) async{
  var res = await http.put('$mainUrl$userPath/$uid',headers: {
    "content-type":"application/json",
    "accept":"application/json",
    "Authorization":"Bearer $key"
  },body: json.encode(userModel));
  if(res.statusCode == 204) return '204';
  else if(res.statusCode == 209) return 'Conflict';
  else return res;
}

Future<String> getBraintreeClientToken(token) async
{
  final response = await http.get('$mainUrl$braintreePath',
      headers: {
        'Authorization':'Bearer $token'
      });
  return response.body;
}

checkOut(String key,double amount,String nonce) async{
  var body={
    'amount':amount.toString(),
    'nonce':nonce
  };
  var res = await http.post('$mainUrl$braintreePath',headers: {
    "content-type":"application/json",
    "accept":"application/json",
    "Authorization":"Bearer $key"
  },body: json.encode(body));
  return res.statusCode == 200 ? true:false;
}