import 'package:braintree_payment/braintree_payment.dart';
import 'package:firebase_auth_ui/firebase_auth_ui.dart';
import 'package:firebase_auth_ui/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_elegant_number_button/flutter_elegant_number_button.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_shopping_app/const/const.dart';
import 'package:flutter_shopping_app/const/utils.dart';
import 'package:flutter_shopping_app/floor/dao/cart_dao.dart';
import 'package:flutter_shopping_app/floor/entity/cart_product.dart';
import 'package:flutter_shopping_app/network/api_request.dart';
import 'package:flutter_shopping_app/state/state_management.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:flutter_riverpod/all.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class CartDetail extends StatefulWidget{
  final CartDAO dao;

  CartDetail({Key key, this.dao}):super(key:key);

  @override
  State<StatefulWidget> createState() => CartDetailState();
}

class CartDetailState extends State<CartDetail>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('Cart Detail',),
      leading: GestureDetector(onTap: () => Navigator.pop(context),
        child: Icon(Icons.arrow_back),),),
    body: StreamBuilder(
       stream: widget.dao.getAllItemInCartByUid(NOT_SIGN_IN),
       builder: (context, snapshot) {
         var items = snapshot.data as List<Cart>;
         return Column(
           children: [
             Expanded(
               child: ListView.builder(
                 itemCount: items == null ? 0 : items.length,
                 itemBuilder: (context,index){
                   return Slidable(
                       child: Card(
                         elevation: 8,
                         margin: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 6.0),
                         child: Container(
                           padding: const EdgeInsets.all(8),
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             crossAxisAlignment: CrossAxisAlignment.center,
                             children: [
                               Expanded(
                               child: ClipRRect(
                               child: Image(
                               image: NetworkImage(items[index].imageUrl),
                                fit: BoxFit.fill),
                                 borderRadius: BorderRadius.all(Radius.circular(4)),
                                ),
                                 flex: 2,
                               ),
                               Expanded(
                                 flex: 6, child: Container(
                                 padding: const EdgeInsets.only(bottom: 8),
                                 child: Column(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                   crossAxisAlignment: CrossAxisAlignment.center,
                                   children: [
                                     Padding(padding: const EdgeInsets.only(left: 8, right: 8),
                                     child: Text(items[index].name,
                                     style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold), maxLines: 2,),
                                     ), //Product Name
                                     Padding(padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                                       child: Row(mainAxisAlignment: MainAxisAlignment.start,
                                         mainAxisSize: MainAxisSize.max,
                                         children: [
                                           Icon(Icons.monetization_on, color: Colors.grey, size: 16),
                                           Container(
                                             margin: const EdgeInsets.only(left: 8),
                                             child: Text('${items[index].price}',
                                             style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                                           )
                                         ],)
                                     ),
                                     Padding(
                                         padding: const EdgeInsets.only(
                                                left: 8, right: 8, top: 8),
                                               child: Row(
                                                 mainAxisAlignment: MainAxisAlignment.start,
                                                 children: [
                                                   Text(
                                                       'Size ${items[index].size}',
                                                       style: TextStyle(
                                                           fontSize: 14,
                                                           fontWeight: FontWeight.bold),
                                                       maxLines: 2),
                                                 ],
                                               ),
                                     ), //Product Price
                                   ],
                                 ),
                               ),),
                               Center(
                                 child: ElegantNumberButton(
                                   initialValue: items[index].quantity,
                                   minValue: 1,
                                   maxValue: 100,
                                   buttonSizeHeight: 20,
                                   buttonSizeWidth: 25,
                                   color: Colors.white38,
                                   decimalPlaces: 0,
                                   onChanged: (value) async{
                                     items[index].quantity = value;
                                     await widget.dao.updateCart(items[index]);
                                   },
                                 ),
                               ),
                             ],
                           ),
                         ),
                       ),
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          secondaryActions: [
                            IconSlideAction(
                              caption: 'Delete',
                              icon: Icons.delete,
                              color: Colors.red,
                              onTap: () async{
                                await widget.dao.deleteCart(items[index]);
                              },
                            ),
                          ],
                   );
                 },
               ),
             ),
             Card(
               child: Padding(
                 padding: const EdgeInsets.all(16),
                 child: Column(
                   children: [
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Text('Total', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                         Text('\$${items.length > 0 ?
                         items.map<double>((m) => m.price * m.quantity).reduce((value, element) => value+element).toStringAsFixed(2)
                             : 0}')
                       ],
                     ),
                     Divider(thickness: 1,),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Text('Delivery charge', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                         Text('\$${items.length > 0 ?
                         (items.map<double>((m) => m.price * m.quantity).reduce((value, element) => value+element)*0.1).toStringAsFixed(2)
                             : 0}')
                       ],
                     ),
                     Divider(thickness: 1,),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Text('Sub Total', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                         Text('\$${items.length > 0 ?
                         ((items.map<double>((m) => m.price * m.quantity).reduce((value, element) => value+element))+items.map<double>((m) => m.price * m.quantity).reduce((value, element) => value+element)*0.1).toStringAsFixed(2)
                             : 0}',style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                       ],
                     ),
                     Container(
                       width: double.infinity,
                       child: RaisedButton(
                         color: Colors.black,
                         onPressed: (){
                           // if user not login, ask login for checkout
                           var user =  FirebaseAuth.FirebaseAuth.instance.currentUser;
                           if(user == null) // login
                               {
                             FirebaseAuthUi.instance().launchAuth([
                               AuthProvider.phone()
                             ]).then((firebaseUser) {
                               // refresh state
                               context.read(userLogged).state = FirebaseAuth.FirebaseAuth.instance.currentUser;
                             }).catchError((e) {
                               if (e is PlatformException) {
                                 if (e.code == FirebaseAuthUi.kUserCancelledError)
                                   showOnlyShackBar(context, 'User cancelled login');
                                 else
                                   showOnlyShackBar(context, '${e.message ?? 'Unknown error'}');
                               }
                             });
                           }
                           else // logout
                               {
                              // Already login
                             var totalPayment = double.parse(items.length > 0 ?
                             ((items.map<double>((m) => m.price * m.quantity).reduce((value, element) => value+element))+items.map<double>((m) => m.price * m.quantity).reduce((value, element) => value+element)*0.1).toStringAsFixed(2)
                                 : 0);
                             FirebaseAuth.FirebaseAuth.instance.currentUser
                             .getIdToken()
                             .then((firebaseToken) async{
                               var clientToken = await getBraintreeClientToken(firebaseToken);
                               var braintreePayment = await BraintreePayment();
                               var data = await braintreePayment.showDropIn(
                                 nonce: clientToken,
                                 amount: totalPayment.toString(),
                                 enableGooglePay: true,
                                 nameRequired: false
                               );
                               var result = await checkOut(firebaseToken, totalPayment, data['paymentNonce']);
                               if(result)
                                 {
                                   //Payment success
                                   Alert(
                                       context: context,
                                       type: AlertType.success,
                                       title: 'PAYMENT SUCCESS',
                                       desc: 'Thank you for purchase',
                                       buttons: [
                                         DialogButton(child: Text('GO BACK'), onPressed: () {
                                           Navigator.pop(context); // Close Dialog
                                           Navigator.pushNamed(context, '/productList');
                                         })
                                       ]
                                   ).show();
                                 }
                               else{
                                 //Failed
                                 Alert(
                                     context: context,
                                     type: AlertType.error,
                                     title: 'PAYMENT FAILED',
                                     desc: result,
                                     buttons: [
                                       DialogButton(child: Text('OK'), onPressed: () {
                                         Navigator.pop(context); // Close Dialog
                                       })
                                     ]
                                 ).show();
                               }
                             });
                           }
                         },
                         child: Text('CHECK OUT', style: TextStyle(color: Colors.white),),
                       ),
                     )
                   ],
                 ),
               ),
             ),
           ],
         );
       },
    ),
    );
  }
}
