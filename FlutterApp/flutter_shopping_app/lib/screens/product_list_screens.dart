import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth_ui/firebase_auth_ui.dart';
import 'package:firebase_auth_ui/providers.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_riverpod/all.dart';
import 'package:flutter_shopping_app/const/utils.dart';
import 'package:flutter_shopping_app/model/UserModel.dart';
import 'package:flutter_shopping_app/model/category.dart';
import 'package:flutter_shopping_app/model/product.dart';
import 'package:flutter_shopping_app/network/api_request.dart';
import 'package:flutter_shopping_app/state/state_management.dart';
import 'package:flutter_shopping_app/widgets/product_card.dart';
import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductListPage extends ConsumerWidget {
  //ignore: top_level_function_literal_block
  final _fetchCategories = FutureProvider((ref) async {
    var result = await fetchCategories();
    return result;
  });

  //ignore: top_level_function_literal_block
  final _fetchProductBySubCategory = FutureProvider.family<List<Product>,int>((ref,subCategoryId) async {
    var result = await fetchProductsBySubCategory(subCategoryId);
    return result;
  });

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context, T Function<T>(ProviderBase<Object, T> provider) watch) {
    var categoriesApiResult = watch(_fetchCategories);
    var productsApiResult = watch(_fetchProductBySubCategory(context.read(subCategorySelected).state.subCategoryId));
    var userWatch = watch(userLogged);

    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: categoriesApiResult.when
          (data: (categories) =>
            ListView.builder
              (itemCount: categories.length,
              itemBuilder: (context, index) {
                return Card(
                  child: Padding(padding: const EdgeInsets.all(8),
                    child: ExpansionTile(
                      title: Row(children: [
                        CircleAvatar(backgroundImage: NetworkImage(
                            categories[index].categoryImg),),
                        SizedBox(width: 30,),
                        categories[index].categoryName.length <= 10 ?
                        Text(categories[index].categoryName) :
                        Text(categories[index].categoryName, style: TextStyle(
                            fontSize: 12),
                        ),
                      ],),
                      children: _buildList(categories[index]),
                    ),),
                );
              },
            ),
            loading: () => const Center(child: CircularProgressIndicator(),),
            error: (error, stack) => Center(child: Text('$error'),)),
      ),
      body:
      SafeArea(
        child: Column(
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      icon:Icon(Icons.menu, size:35, color:Colors.black),
                      onPressed: () => _scaffoldKey.currentState.openDrawer()),
                      Text('SHOPPING APP', 
                        style: 
                        TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon:Icon(Icons.phone_android , size:35, color:Colors.black),
                        onPressed: () async{
                          const url = 'https://www.youtube.com/watch?v=2TdcPQuxafU&ab_channel=Dressingthedude';
                          if (await  canLaunch(url)){
                            await launch(url);
                          } else{
                            throw 'Could not launch $url';
                          }
                        },
                      ),
                      Row(
                          children: [
                            FutureBuilder(
                              future: _checkLoginState(context),
                              builder: (context,snapshot){
                              var user = snapshot.data as FirebaseAuth.User;
                              return IconButton(
                                icon: Icon(user == null ? Icons.account_circle_outlined : Icons.exit_to_app,
                                size:35, color:Colors.black),
                                onPressed: () => processLogin(context),
                              );
                            }),
                        IconButton(
                          icon:Icon(Icons.shopping_bag_outlined, size:35, color:Colors.black),
                          onPressed: () => Navigator.of(context).pushNamed('/CartDetail'),
                        ),
                      ],
                    ),
                ],
              ),
              Row(children: [
                Column(children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.amberAccent,
                    child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Center(
                          child: Text(
                              '${context.read(subCategorySelected).state.subCategoryName}'),
                        )
                    ),
                  )
                ],)
              ],),
            ],
          ),
          Expanded(
              child: productsApiResult.when(
                  loading: () => const Center(child: CircularProgressIndicator(),
                  ),
                  error: (error, stack) => Center(child: Text('$error'),
                  ),
                  data: (products) => GridView.count(
                    crossAxisCount: 2,
                    shrinkWrap: true,
                    childAspectRatio: 0.46,
                    children: products.map((e) => ProductCard(product:e)).toList(),
                  )
              ),
          )
        ],
      ),
      ),
    );
  }

  _buildList(MyCategory category) {
    var list = new List<Widget>();
    category.subCategories.forEach((element) {
      list.add(Padding(padding: const EdgeInsets.all(8),
        child: Text(element.subCategoryName,
          style: TextStyle(fontSize: 12),),));
    });
    return list;
  }

  Future<FirebaseAuth.User> _checkLoginState(context) async {
    if(FirebaseAuth.FirebaseAuth.instance.currentUser != null)
      {
        FirebaseAuth.FirebaseAuth.instance.currentUser.getIdToken()
            .then((token) async {
              print('Token $token');
              var result = await findUser( FirebaseAuth.FirebaseAuth.instance.currentUser.uid,
              token.toString());
              if(result =='User Not found')
                {
                  //Register code
                  Navigator.pushNamed(context, "/registerUser");
                }
              else
                {
                  var userModel = UserModel.fromJson(json.decode(result));
                  FirebaseMessaging _messaging = new FirebaseMessaging();
                  _messaging.getToken().then((fcmToken) async{
                    userModel.token = fcmToken; //assign token for user
                    var updateResult = await updateTokenApi(token, userModel.uid, userModel);
                  });
                }
        });
      }
    return FirebaseAuth.FirebaseAuth.instance.currentUser;
  }

  bool loading = false;

  Future<bool> saveFile(String url,String fileName) async {
    Directory directory;
    try {
      if (Platform.isAndroid) {
        if(await _requestPermission(Permission.storage)){
          directory = await getExternalStorageDirectory();
          print(directory.path);
        } else{
          return false;
        }
      } else {

      }
      } catch(e){

    }
    return false;
  }

  Future<bool> _requestPermission(Permission permission) async{
    if(await permission.isGranted){
      return true;
    } else {
      var result = await permission.request();
      if(result == PermissionStatus.granted){
        return true;
      } else {
        return false;
      }
    }
  }

  downloadFile() async {
    setState(() {
      loading = true;
    });

    bool downloaded = await saveFile("https://www.youtube.com/watch?v=2TdcPQuxafU&ab_channel=Dressingthedude", "video.mp4");
    if (downloaded) {
      print("File Downloaded");
    } else {
      print("Problem Downloading File");
    }

    setState(() {
      loading = false;
    });
  }

  processLogin(BuildContext context) async {
    var user =  FirebaseAuth.FirebaseAuth.instance.currentUser;
    if(user == null) // login
        {
      FirebaseAuthUi.instance().launchAuth([
        AuthProvider.phone()
      ]).then((firebaseUser) {
        // refresh state
        context.read(userLogged).state = FirebaseAuth.FirebaseAuth.instance.currentUser;
      }).catchError((e) {
        if (e is PlatformException) {
          if (e.code == FirebaseAuthUi.kUserCancelledError)
            showOnlyShackBar(context, 'User cancelled login');
          else
            showOnlyShackBar(context, '${e.message ?? 'Unknown error'}');
        }
      });
    }
    else // logout
    {
      var result = await FirebaseAuthUi.instance().logout();
      if (result)
        {
          showOnlyShackBar(context, 'Logout successfully');
          // refresh
          context.read(userLogged).state = FirebaseAuth.FirebaseAuth.instance.currentUser;
        }
      else
        showOnlyShackBar(context, 'Logout error');
    }
  }

  void setState(Null Function() param0) {
    ProductListPage createState()=> ProductListPage();
  }
}
